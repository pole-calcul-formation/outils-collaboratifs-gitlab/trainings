# Docker

Avant d'aller plus loin, nous allons **très rapidement** présenter l'outil Docker qui est très utilisé dans le process d'intégration continue.

**Docker** : une plateforme logicielle open source permettant de créer, de déployer et de gérer des containers d’applications virtualisées sur un système d’exploitation.

**En pratique** : vous allez pouvoir faire tourner des applications (python, g++, R, latex ...) dans un environnement de votre choix (dans certaines limites) sur la machine de votre choix.

Par exemple, je peux sur mon portable macos lancer une session 'linux ubuntu' via la commande

```bash
docker run -ti ubuntu
# ou debian
docker run -ti debian:latest
# ou ...
```

Dans les exemples ci-dessus, j'ai crée à partir d'une image (le modèle, ubuntu ou debian dans notre cas) un container (une "instance" de l'image) sur lequel je peux exécuter des commandes linux, compiler du code etc.

**Quel intérêt ?**
* Accès à un large choix de systèmes et d'outils potentiellement non disponible sur votre OS
* Facilite la reproduction des environnements
* ...

## Comment récupérer ou construire une image ?

De nombreuses images sont disponibles sur https://hub.docker.com/ et peuvent être chargées sur votre machine via la commande 

```console
docker pull <image_name>
```

Vous pouvez également construire vos propres images, à partir d'un 'Dockerfile', fichier décrivant les packages à installer sur le système.

```
# Exemple de fichier Dockerfile
FROM ubuntu:18.04
RUN apt update  && apt install -y -qq \
    cmake \
    git-core
```

puis en ligne de commande 
```console
docker build -t my_image_name path_to_dockerfile # construction de l'image
docker run -ti my_image_name /bin/bash           # démarrage d'un container
```

## Quelques commandes utiles

* liste des images disponibles sur votre machine

```
docker images
```

* liste des containers existants sur votre machine
```
docker ps -a
```

* démarrer une image en montant un répertoire local dans votre container (i.e. partage de fichiers entre votre machine et le container)

```
docker run -v path_source:path_container -ti image_name
```

- path_source = chemin absolu vers le répertoire sur votre machine
- path_container = choix du nom dans le container du répertoire monté



Voila pour ce bref aperçu de Docker. Pour l'intégration continue, les quelques notions ci-dessus sont largement suffisantes.
Si vous souhaitez aller plus loin, vous trouverez toutes les infos nécessaires en ligne (https://docs.docker.com/ par exemple).

[Retour à l'intégration continue ...](./ci.md)
