# Trainings - Intégration continue dans un projet gitlab.

[[_TOC_]]

**Objectifs :**
* bien comprendre les principes de l'intégration et du déploiement continue (CI/CD)
* maitriser les outils (gitlab-ci) et être capable de les mettre en place dans un projet gitlab.

Pour cela, nous allons vous montrer en mode 'tutoriel' différentes étapes. A vous 
de les appliquer au fur et à mesure dans votre projet.


Pour chaque partie (training ou démo selon le temps disponible ...), nous indiquerons la référence vers un fichier modèle à copier dans le .gitlab-ci.yml de votre projet.

## Training/Démo 1 - Premier exemple de job

Un job minimal, pour illustrer le fonctionnement de la CI.

Activez la CI en utilisant dans n'importe quel projet le [modèle demo0.yml](./templates/demo0.yml) comme source pour votre .gitlab-ci.yml

A voir :

- le menu Build
- le pipeline
- le suivi d'un job

## Trainings/Démo 2 - Création d'un pdf

Voir le projet [Demos/Exemple intégration continue - 1](https://gricad-gitlab.univ-grenoble-alpes.fr/pole-calcul-formation/outils-collaboratifs-gitlab/demos/ci-example-basics)

:checkered_flag: Fin  démo 2 - Retour au cours

## Trainings/Démo 3 - Création et mise en ligne d'une page web

Voir la suite de la démo dans le projet [Demos/Exemple intégration continue - 1](https://gricad-gitlab.univ-grenoble-alpes.fr/pole-calcul-formation/outils-collaboratifs-gitlab/demos/ci-example-basics), partie "Mise en ligne et création d'une page web : gitlab-pages"

:checkered_flag: Fin  démo 3 - Retour au cours

## Trainings/Démo 4 - Les runners

**Runner** : une machine, un hôte sur lequel vont être exécutées les tâches d’intégration continue.

Jusqu’à présent, nous ne nous sommes pas interessés à la machine hôte des simulation, le **runner**.

Sans le savoir, vous avez utilisé un runner par défaut, partagé par tous les projets du groupe.

Pour les runners, différentes options sont possibles :

* utiliser le runner par défaut (shared) disponible pour l'ensemble des projets de la plateforme.

  :warning: à n’utiliser que pour les phases de mise en place et de test !

* définir ses propres runners (autant que vous voulez) et les associer à un groupe, des projets etc.
Nous vous recommandons bien sur fortement cette deuxième méthode.


#### Comment ?

Vous pouvez utiliser n'importe quelle machine à votre disposition pour installer un runner : votre poste, une machine du labo ou autre.

Dans cette démo, nous allons ajouter un runner sur une machine virtuelle disponible via Nova (Nova ?? Mais si souvenez-vous, le service GRICAD de machines virtuelles à la demande).

Trois étapes :

* Déclarer votre runner via l'interface web
* Installer gitlab-runner et docker sur la machine hôte
* Enregistrer votre 'runner' auprès de votre groupe ou projet (ligne de commande, sur la machine hôte)

Pour l'installation, tout dépend de votre système, mais c'est assez classique et sans difficulté. Voir par exemple [gitlab runner pour ubuntu/debian](https://docs.gitlab.com/runner/install/linux-repository.html)

Une fois l'installation terminée, il faut enregistrer le runner via la commande :

```bash
> gitlab-runner register
...
```
Les informations à fournir sont indiqués dans les settings de votre projet : Settings du projet → CI/CD → Runner settings.

Une fois la procédure achevée, vous verrez apparaitre le runner dans la liste et il sera disponible pour faire tourner vos jobs.

#### Remarques 

* Le runner peut-être installé au niveau du projet ou du groupe
* Si il est installé pour le groupe, il est disponible pour tous les projets du groupe.

:checkered_flag: Fin  démo 4 - Retour au cours

## Trainings/Démo 5 - CI/CD dans un code de calcul

Tout est décrit dans le projet [Demos/Exemples intégration continue - CI CD pour un code de calcul
](https://gricad-gitlab.univ-grenoble-alpes.fr/pole-calcul-formation/outils-collaboratifs-gitlab/demos/ci-example-advanced)





