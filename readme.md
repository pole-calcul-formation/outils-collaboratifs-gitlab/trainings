# Trainings Gitlab et Git

Matériel pour les travaux pratiques du cours "Gestion de projet et outils collaboratifs  - Utilisation de la plate-forme Gricad-gitlab"


[[_TOC_]]


<p xmlns:cc="http://creativecommons.org/ns#" >This work is licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>



## Gitlab - Connexion

* Connectez vous sur la plate-forme via votre identifiant agalan. Si vous n'en avez pas ... faites nous signe.
* Visitez le menu "Preferences" de votre compte et complétez tout ce qui vous parait utile, en particulier :
    * le champ organization avec le nom de votre laboratoire
    * les adresses mail secondaires
    * le réglages des notifications : visualisez les différentes possibilités et fixez un mode par défaut.

        :thumbsup: "disabled" par défaut pour éviter l'envoi intempestif d'emails ...

* Explorez les différents menus, familiarisez vous avec l'interface, la doc en ligne etc

Nous reviendrons plus tard sur la partie concernant les clés ssh.

:checkered_flag: Fin training 1 - Retour au cours 

## Gitlab - Projets et groupes

 **Objectifs** : 15 minutes pour manipuler groupes et projets et naviguer dans les différents menus.

 **Consignes** : n'hésitez pas à tester et cliquer partout !
  
Quelques pistes :

* Créez un projet privé dans votre espace personnel.
* Visitez, testez les différents onglets du menu  "Settings" de votre nouveau projet.
* Rendez votre projet visible (interne) et activez le bouton "request access" qui permettra à d'autres personnes de demander à rejoindre votre projet.
* Demandez à rejoindre un autre projet/Ajoutez des personnes de la salle à votre projet avec un rôle "guest".
* Créez / supprimez / déplacez des projets
* Gérez des inscriptions, ajoutez des participants 
    
    :thumbsup: Pensez au champ *Access expiration date* (6 mois par exemple).

:checkered_flag: Fin training 2 - Retour au cours

## Git - Premiers pas

Pour cette partie nous abandonnons le serveur gricad-gitlab : tout le travail est à faire sur votre machine, en ligne de commande.

**Objectifs** 
- Création d'un dépôt git 
- Manipulation des commandes de bases vues dans le cours jusqu'ici

:point_right: n'hésitez pas à tester et expérimenter toutes les commandes


Utilisez git en ligne de commande (dans un terminal) pour répondre aux points listés ci-dessous.
  
1. Créez un dépôt git via la commande 

```
git init
```

2. Configurez vos paramètres git via la commande

```
git config
```

Vérifiez le contenu du fichier $HOME/.gitconfig.

3. Créez un fichier Readme.md et ajoutez le au dépôt. Observer les sorties de 'git status, git log' entre chaque étape.

4. Ajoutez puis supprimez d'autres fichiers, répertoires. Bref, testez toutes les commandes vues dans le cours jusqu'à présent.

Vous noterez au passage que la commande git status fournit les lignes à taper pour corriger l'état des derniers fichiers modifiés.

:checkered_flag: Fin training 3 Git premiers pas - Retour au cours

## Git - Les branches

Dans votre repository local

* Créez une nouvelle branche.
* Faites la évoluer (nouveaux fichiers, modifications etc).
* Fusionnez cette branche dans la branche principale
* Supprimez la branche après la fusion.

:point_right: Démonstration pour changer la branche par défaut d'un projet

:checkered_flag: Fin training 4 Git - Branches - Retour au cours

## Git et gitlab - Première utilisation d'un dépôt remote

Nous allons voir dans cette partie comment établir un lien entre un repository git local (sur votre laptop) et un projet Gitlab.

Vous disposez actuellement de deux repositories git crées lors des exercices précédents :

* Un repository git local, sur votre machine, (crée via la commande git init)
* Un ou des projets créés au training 2.


### Clés ssh

Avant de passer à la suite il est nécessaire de poser votre clé ssh publique sur la plateforme.

#### Pourquoi ?

Clés ssh : un outil nécessaire pour gérer les connexions sécurisées entre votre machine locale et le serveur gitlab.

**Objectif** : disposer d'un jeu de deux clés

* une clé privée sur la machine sur laquelle vous travaillez (votre portable, une machine de la salle de formation ...)
* une clé publique sur la machine cible (le serveur gricad-gitlab dans notre cas présent)

A chaque tentative d'échange entre votre machine et le serveur gitlab (par exemple git push ou pull)
une vérification de la correspondance entre votre clé locale (privée) et la clé distante (publique) sera effectuée.
Le résultat de cette vérification permettra la connexion (ou pas).

#### Comment ?

Pour que cela fonctionne il faudra :

* [Générer une paire de clés](#generer-une-paire-de-cles-ssh) SSH (ou vérifier si vous n'en avez pas déja une).
* [Copier votre clé publique](#copier-votre-cle-publique) sur le serveur gricad-gitlab.

#### Générer une paire de clés SSH

:warning: Vérifiez le contenu du répertoire `.ssh/` sur votre machine en exécutant dans un terminal les commandes suivantes :

```console
$  cd $HOME
$  ls .ssh/
id_rsa id_rsa.pub
```

Si il contient deux fichiers id_rsa et id_rsa.pub passez à l'[étape suivante](#copier-votre-cle-publique).

Dans le cas contraire, il faut générer des clés via la commande ssh-keygen :

```console
$  ssh-keygen -t rsa
```

- Répondez simplement aux questions en **laissant les valeurs par défaut**, sauf si vous savez ce que vous faites.
- Vous **devez** fournir une _passphrase_ **non vide** ET vous en souvenir.

Après cette étape, deux fichiers ont été générés :

* `.ssh/id_rsa` : votre clé privée et __strictement personnelle__
* `.ssh/id_rsa.pub` : la clé publique correspondante à votre clé privée.


##### Authentification - Remarque importante

Lors de tentatives de connexion, si :
- votre __password__ est demandé, utilisez vos identifiants Agalan (comme pour la connexion au web gricad-gitlab)

- votre __passphrase__ est demandée, utilisez celle fournie pour votre clé SSH

#### Copier une clé publique sur le serveur

Il faut maintenant transférer votre clé publique vers le serveur gricad-gitlab.

Trouvez le menu **SSH keys** sur la page "Preferences" de votre compte sur gricad-gitlab.

Copiez le contenu du fichier .ssh/id_rsa.pub dans le champ prévu à cet effet.

#### Pour plus de détails

* [La doc gitlab](https://gricad-gitlab.univ-grenoble-alpes.fr/help/user/ssh.md)
* [Le cours d'introduction à linux](https://pole-calcul-formation.gricad-pages.univ-grenoble-alpes.fr/ced)

### Connexion des deux repositories

* En local (Terminal), ajoutez votre projet gricad-gitlab dans les remotes de votre repository
* Transférez votre branche principale locale vers gricad-gitlab.
* Vérifiez sur la page web de votre projet que tout s'est bien passé. Explorez en particulier les différents menus de l'onglet **Code**.
Vous devriez notamment retrouver l'historique de vos commits.

![](./images/gitlab_git_code.png)

:checkered_flag: Fin training 5 - Git première utilisation d'un dépôt remote - Retour au cours

## Git et gitlab, opérations en ligne

### Markdown et Web IDE

Comme indiqué dans le cours, vous pouvez éditer en ligne l'ensemble des fichiers de votre repository via le menu **Web IDE**.

Via ce menu, vous pouvez gérer entièrement votre dépôt git et avez accès à la plupart des opérations git vues et utilisées en ligne de commande.

Familiarisez vous avec le markdown et le WebIDE : 

- Editez vos fichiers avec le Web IDE
- Validez (Create commit)

Les opérations en ligne ci-dessus sont équivalentes à la séquence de commandes locales suivante:

```
cd votre_projet
git pull
emacs readme.md
git commit -a -m "blabla"
git push
```

### Notifications et envoi d'emails pour les commits

Il est possible de recevoir un email à chaque push dans le repository :

Settings -> Integrations -> emails on push : ajoutez votre email dans la liste des destinataires

:warning: cette option est indépendante des notifications

### Cloner et échanger avec un dépôt distant
 
A partir d'ici : tout le monde va travailler avec un seul projet sur gitlab, common-training, dans le groupe sandbox.

Vous allez maintenant essayer de reproduire le workflow présenté plus haut dans le cours : 

- copie d'un dépôt remote, 
- modifications locales, 
- transfert vers le serveur et prise en compte des modifications des autres utilisateurs.

**Objectifs** : utilisation des commandes git clone, pull, push, commit, remote, status, merge ...
  

1. Dans un terminal, clonez le dépôt common-training de gricad-gitlab (git clone) en utilisant le protocole "ssh".

2. Dans ce dépôt, créez un répertoire à votre nom (login agalan de préférence) contenant un fichier quelconque. Commitez ce fichier

   (git clone, add, commit, status).

3. Poussez vos modifications vers gricad-gitlab/sandbox/common-training.
 
   (git add, commit, status, remote, pull, push).

:warning:  avant de faire un push il est indispensable de récupérer le dernier état du dépot origin/master via la commande git pull ! 

En particulier pour obtenir localement les modifications ajoutées par les autres participants et ne pas les écraser.

* Observez entre chaque étape les sorties des commandes log, status et l'affichage de la page web du projet.
* Vérifiez que vous recevez bien un email à chaque mise à jour du dépôt sur gricad-gitlab.


:checkered_flag: Fin training 6 Git/Gitlab opérations en ligne - Retour au cours

## Git - Workflow

Dans cette partie nous allons décrire la séquence d'opérations à mettre en place pour résoudre "proprement" un problème quelconque dans un code, afin d'illustrer l'intérêt des issues et des merge-requests.
Tout le monde doit travailler dans le projet common-training.

### Déclaration et classement d'un problème

* Utilisez les outils du menu Plan (issues, milestones ...) pour déclarer des problèmes.
* Créez des labels, des milestones, testez les différents sous-menus.
* Attribuez les issues à un membre du projet
* Clore via un message de commit un des bugs qui vous a été attribué.

Pour aller plus loin : https://docs.gitlab.com/ce/user/project/issues
  
### Git et gitlab - Merge requests

* Déclarez un autre problème et créez un fork ou une branche dédiée à la résolution de ce problème.
* Associez une merge-request à cette branche
* Faites évoluer la branche puis mergez la dans la branche principale

Allons-y ...

* Déclarez un problème imaginaire, une nouvelle idée de fonctionnalité via une issue
Cette première étape indispensable, permettra :
    * la prise de connaissance et le suivi du bug par l'ensemble des développeurs,
    * l'archivage des éventuels commentaires, discussions des solutions, 
    * le classement (label), estimation de durée (milestone), dépendances etc.
 
* Créez un fork du projet dans votre namespace
    :arrow_right: la branche où vous allez effectuer des développements spécifiques, pour résoudre le bug, sans interférer avec le projet commun.

* Faites quelques modifs, commits etc dans ce fork, pour faire évoluer sa branche principale.

* Dans votre fork, démarrez une *merge-request* ayant pour source votre fork et pour cible la branche main du projet common-training.
Notez au passage les différents champs, la possibilité d'attribuer des labels, des milestones etc.
Pour assurer le suivi du problème, faîtes référence dans ces champs à l'issue que vous venez de déposer pour signaler le bug.

Rappel : référence possible en markdown à une issue :
```
#numero_issue ou groupe/projet#numero_issue
```    
Conséquences :

:arrow_right: déclaration aux autres développeurs de l'intention de fusionner votre branche, appel à des commentaires, revue de code, 

:arrow_right: suivi dans le tableau de bord de l'évolution du traitement de l'issue.

Dans les commentaires, issues ou autres vous pouvez également faire référence aux merge-requests via la syntaxe markdown suivante :

``` 
    !numero_merge_request   
```

* Suivez les autres merge-requests du projet common-training, faites des review, commentaires

* Sur la page de votre merge-request

    * Résolvez les conflits (interactive mode, commit ...), à vous de choisir
    * Validez et fusionnez (Bouton merge)
    * Assurez vous que l'issue de départ a automatiquement été marquée comme "closed" via le dernier message de commit ou un commentaire du merge-request. 

![](./images/gitlab_mr.jpg)

Si vous cliquez sur *resolve conflicts*, vous pouvez tenter de résoudre ceux-ci en ligne :

![](./images/gitlab_mr_conflict.jpg)


Conséquences : vos modifs (fork) ont été intégrées à la branche master du projet commun.

* Pour finir :
    * supprimez votre fork (:warning: le projet dans votre espace personnel, pas le commun), settings/General/advanced settings/Remove project
    * supprimez votre merge-request.

    
:checkered_flag: Fin du training 7 - Gitlab Workflow - Retour au cours

## Complément : utilisation de l'API gitlab

Il est tout à fait possible d'interagir avec la plateforme gricad-gitlab via des scripts python.
C'est un outil très pratique pour automatiser certaines actions.


Pour cela il est nécessaire de 
- posséder un token personnel
- créer un fichier de config contenant ce token
- utiliser le package python-gitlab pour se connecter à la plateforme et interagir.

Dans cette démo nous allons :

- lister les caractéristiques d'un utilisateur
- créér un projet et lui ajouter une liste de personne

tout cela via un script python.

#### Installation du package

```
python3 -m pip install python-gitlab
```

#### Création d'un token personnel

Sur gitlab, allez dans votre menu "Edit Profile" (en haut à droite) puis sélectionner dans la colonne de gauche Access Token.
Suivez ensuite les instructions pour créér votre token et notez le bien.

#### Fichier de config

Créez sur votre machine personnel un fichier python-gitlab.cfg, contenant les lignes suivantes :
```
[global]
default = gricad
ssl_verify = true
timeout = 5

[gricad]
url = https://gricad-gitlab.univ-grenoble-alpes.fr
private_token = VOTRE_TOKEN
api_version = 4
```

#### Script python

Créez un script python (ou utilisez un notebook) avec les contenus suivants

```python
# Connexion à la plateforme

import gitlab
gl =gitlab.Gitlab.from_config('gricad', ['./python-gitlab-real.cfg'])

# Récupération d'un utilisateur et de ses infos
user_id = XXXX # mettez ici votre id
user = gl.users.get(user_id)
for att in user.attributes:
    print(a, user.attributes[a])

# Création d'un projet dans le groupe outils-collaboratifs-gitlab
group = gl.groups.get(38947)
print(group.name, group.id)
project = gl.projects.create({'name': 'projet_test', 'namespace_id': group.id})

# Ajout d'une liste d'utilsateurs dans ce projet, avec le rôle 'developpeur'
# jusuq'à la date du 29/11/2022.
#
# Les utilisateurs doivent bien sur déja exister
users = ['nom1', 'nom2'] # A ADAPTER !
for uname in users:
    u = gl.users.list(username=uname)[0]
    print(u.email, u.id)
    project.invitations.create(
     {
        "email": u.email,
        "access_level": gitlab.const.AccessLevel.DEVELOPER,
        "expires_at": "2022-11-29" })
    project.save()

```



## Intégration continue

[trainings](./ci.md)




  





